/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.packets;

import com.shinigota.ProjectBurst.net.GameClient;
import com.shinigota.ProjectBurst.net.GameServer;

public class PacketNewBullet extends Packet {
	private float x, y, angle;
	private int id, dmg;
	
	public PacketNewBullet(int id, float x, float y, float angle, int dmg) {
		super(12);
		this.id = id;
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.dmg = dmg;
	}

	public PacketNewBullet(byte[] data) {
		super(12);
		String[] array = readData(data).split(";");
		this.id = Integer.parseInt(array[0]);
		this.x = Float.parseFloat(array[1]);
		this.y = Float.parseFloat(array[2]);
		this.angle = Float.parseFloat(array[3]);
		this.dmg = Integer.parseInt(array[4]);
	}

	@Override
	public void writeData(GameClient client) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void writeData(GameServer server) {
		server.sendDataToAllClients(this.getData());		
	}

	@Override
	public byte[] getData() {
		return ("12" + this.id + ";" + this.x + ";" + this.y + ";" + this.angle + ";" + this.dmg).getBytes();
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getAngle() {
		return angle;
	}

	public int getDmg() {
		return dmg;
	}

	public int getId() {
		return id;
	}
}
