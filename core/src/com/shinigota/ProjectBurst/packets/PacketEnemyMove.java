/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.packets;

import com.shinigota.ProjectBurst.net.GameClient;
import com.shinigota.ProjectBurst.net.GameServer;

public class PacketEnemyMove extends Packet{
	private int id;
	private float x, y, angle;
	
	public PacketEnemyMove(byte[] data) {
		super(07);
		String[] dataArray = readData(data).split(";");
		this.id = Integer.parseInt(dataArray[0]);
		this.x = Float.parseFloat(dataArray[1]);
		this.y =  Float.parseFloat(dataArray[2]);
		this.angle = Float.parseFloat(dataArray[3]);
	}
	

	public PacketEnemyMove(int id, float x, float y, float angle) {
		super(07);
		this.id = id;
		this.x = x;
		this.y = y;
		this.angle = angle;
	}


	@Override
	public void writeData(GameClient client) {
		client.sendData(this.getData());
	}

	@Override
	public void writeData(GameServer server) {
		server.sendDataToAllClients(this.getData());
	}

	public int getId() {
		return id;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	
	public float getAngle() {
		return angle;
	}


	@Override
	public byte[] getData() {
		return ("07" + this.id + ";" + this.x + ";" + this.y + ";" + this.angle).getBytes();
	}
}
