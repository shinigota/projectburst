/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.packets;

import com.shinigota.ProjectBurst.net.GameClient;
import com.shinigota.ProjectBurst.net.GameServer;

public class PacketBulletMoved extends Packet {
	private int id;
	private float x, y;
	
	public PacketBulletMoved(int id, float x, float y) {
		super(13);
		this.id = id;
		this.x = x;
		this.y = y;
	}

	public PacketBulletMoved(byte[] data) {
		super(13);
		String[] array = readData(data).split(";");
		this.id = Integer.parseInt(array[0]);
		this.x = Float.parseFloat(array[1]);
		this.y = Float.parseFloat(array[2]);
	}
	@Override
	public void writeData(GameClient client) {
		// TODO Auto-generated method stub
	}

	@Override
	public void writeData(GameServer server) {
		server.sendDataToAllClients(this.getData());		
	}

	@Override
	public byte[] getData() {
		return ("13" + this.id + ";" + this.x + ";" + this.y).getBytes();
	}

	public int getId() {
		return id;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	
}
