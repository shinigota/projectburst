/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.packets;

import com.shinigota.ProjectBurst.net.GameClient;
import com.shinigota.ProjectBurst.net.GameServer;

public class PacketRotate extends Packet{

	private String username;
	private float angle;
	
	public PacketRotate(byte[] data) {
		super(03);
		String[] dataArray = readData(data).split(";");
		this.username = dataArray[0];
		this.angle =  Float.parseFloat(dataArray[1]);
	}
	

	public PacketRotate(String username, float angle) {
		super(03);
		this.username = username;
		this.angle = angle;
	}


	@Override
	public void writeData(GameClient client) {
		client.sendData(this.getData());
	}

	@Override
	public void writeData(GameServer server) {
		server.sendDataToAllClients(this.getData());
	}

	public String getUsername() {
		return username;
	}
	
	@Override
	public byte[] getData() {
		return ("03" + this.username + ";" + this.angle).getBytes();
	}

	public float getAngle() {
		return angle;
	}
	
}
