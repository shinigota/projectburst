/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.packets;

import com.shinigota.ProjectBurst.net.GameClient;
import com.shinigota.ProjectBurst.net.GameServer;

public class PacketPlayerShoot extends Packet {

	private String username;
	private float x, y, angle;
	private int dmg, projectiles, spread;
	
	public PacketPlayerShoot(String username, float x, float y, float angle, int dmg, int projectiles, int spread) {
		super(10);
		this.username = username;
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.dmg = dmg;
		this.projectiles = projectiles;
		this.spread = spread;
	}
	
	public PacketPlayerShoot(byte[] data) {
		super(10);
		String[] dataArray = readData(data).split(";");
		this.username = dataArray[0];
		this.x = Float.parseFloat(dataArray[1]);
		this.y = Float.parseFloat(dataArray[2]);
		this.angle = Float.parseFloat(dataArray[3]);
		this.dmg = Integer.parseInt(dataArray[4]);
		this.projectiles = Integer.parseInt(dataArray[5]);
		this.spread = Integer.parseInt(dataArray[6]);
	}

	@Override
	public void writeData(GameClient client) {
		client.sendData(this.getData());
	}

	@Override
	public void writeData(GameServer server) {
		// TODO Auto-generated method stub

	}

	@Override
	public byte[] getData() {
		return ("10" + this.username + ";" + this.x + ";" + this.y + ";" + this.angle + ";" + this.dmg + ";" + this.projectiles + ";" + this.spread).getBytes();
	}

	public String getUsername() {
		return username;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getAngle() {
		return angle;
	}

	public int getDmg() {
		return dmg;
	}

	public int getProjectiles() {
		return projectiles;
	}

	public int getSpread() {
		return spread;
	}
}
