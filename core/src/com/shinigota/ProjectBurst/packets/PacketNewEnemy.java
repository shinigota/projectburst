/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.packets;

import com.shinigota.ProjectBurst.net.GameClient;
import com.shinigota.ProjectBurst.net.GameServer;

public class PacketNewEnemy extends Packet{
	private int entityId, width, height;
	private float x, y;
	
	public PacketNewEnemy(int entityId, float x, float y, int width, int height) {
		super(06);
		this.entityId = entityId;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public PacketNewEnemy(byte[] data) {
		super(06);
		String[] array = readData(data).split(";");
		this.entityId = Integer.parseInt(array[0]);
		this.x = Float.parseFloat(array[1]);
		this.y = Float.parseFloat(array[2]);
		this.width = Integer.parseInt(array[3]);
		this.height = Integer.parseInt(array[4]);
	}


	@Override
	public void writeData(GameClient client) {
		// TODO Auto-generated method stub
	}

	@Override
	public void writeData(GameServer server) {
		server.sendDataToAllClients(this.getData());
	}

	@Override
	public byte[] getData() {
		return ("06" + entityId + ";" + x + ";" + y + ";" + width + ";" + height).getBytes();
	}

	public int getEntityId() {
		return entityId;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	
}
