/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.packets;

import java.net.InetAddress;

import com.shinigota.ProjectBurst.net.GameClient;
import com.shinigota.ProjectBurst.net.GameServer;

public abstract class Packet {
	public static enum PacketTypes {
		INVALID(-1), 
		CONNECT(00), 
		DISCONNECT(01), 
		MOVE(02), 
		ROTATE(03), 
		SERVERINFO(04), 
		ASKINFO(05), 
		NEWENEMY(06), 
		ENEMYMOVE(07), 
		PLAYERSHOOT(10), 
		ENEMYDIED(50), 
		NEWBULLET(12), 
		BULLETMOVED(13), 
		DELETEENTITY(11),
		ENTITYMOVE(14);
		
		private int id;

		private PacketTypes(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}
	}

	public byte packetId;

	public Packet(int packetId) {
		this.packetId = (byte) packetId;
	}
	
	public abstract void writeData(GameClient client);
	
	public abstract void writeData(GameServer server);
	
	public void writeDataTo(GameServer server, PacketServerInfo packet, InetAddress address, int port) {
		server.sendData(packet.getData(), address , port);
	}
	
	public String readData(byte[] data) {
		String message = new String(data).trim();
		return message.substring(2);
	}
	
	public abstract byte[] getData();
	
	public static PacketTypes getType(int id) {
		for(PacketTypes p : PacketTypes.values())
			if(p.getId() == id)
				return p;
		return PacketTypes.INVALID;
	}
	
	public static PacketTypes getType(String id) {
		try {
			return getType(Integer.parseInt(id));
		} catch(NumberFormatException e) {
			return PacketTypes.INVALID;
		}
	}
}
