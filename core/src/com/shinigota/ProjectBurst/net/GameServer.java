/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JOptionPane;

import com.shinigota.ProjectBurst.ProjectBurst;
import com.shinigota.ProjectBurst.entities.Bullet;
import com.shinigota.ProjectBurst.entities.Enemy;
import com.shinigota.ProjectBurst.entities.Entity;
import com.shinigota.ProjectBurst.entities.net.PlayerNet;
import com.shinigota.ProjectBurst.packets.Packet;
import com.shinigota.ProjectBurst.packets.Packet.PacketTypes;
import com.shinigota.ProjectBurst.packets.PacketConnect;
import com.shinigota.ProjectBurst.packets.PacketDisconnect;
import com.shinigota.ProjectBurst.packets.PacketNewBullet;
import com.shinigota.ProjectBurst.packets.PacketNewEnemy;
import com.shinigota.ProjectBurst.packets.PacketPlayerMove;
import com.shinigota.ProjectBurst.packets.PacketPlayerShoot;
import com.shinigota.ProjectBurst.packets.PacketRotate;
import com.shinigota.ProjectBurst.packets.PacketServerInfo;
import com.shinigota.ProjectBurst.world.World;


public class GameServer extends Thread{
	private GameServerWorldHandler worldHandler;
	private DatagramSocket socket;
	private World world;
	private ServerOptions serverOptions;
	public boolean refreshAi;
	
	private List<PlayerNet> connectedPlayers;

	public GameServer(World world, ServerOptions serverOptions) {
		this.world = world;
		this.serverOptions = serverOptions;
		
		this.connectedPlayers = new CopyOnWriteArrayList<PlayerNet>();
		
		try {
			this.socket = new DatagramSocket(ProjectBurst.DEF_PORT);
		} catch (SocketException e) {
			e.printStackTrace();
		}
		
		this.worldHandler = new GameServerWorldHandler(this, world);
		this.worldHandler.start();
		this.refreshAi = true;
	}
	
	public void run() {
		while(true) {
			byte[] data = new byte[2048];
			DatagramPacket packet = new DatagramPacket(data, data.length);
			
			try {
				socket.receive(packet);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			this.parsePacket(packet.getData(), packet.getAddress(), packet.getPort());
			
		}
	}
	
	private void parsePacket(byte[] data, InetAddress address, int port) {
		String message = new String(data).trim();
		PacketTypes type = Packet.getType(message.substring(0, 2));
		Packet packet = null;
		switch(type){
		case INVALID:
			break;
		case CONNECT:
			packet = new PacketConnect(data);
			PlayerNet player = new PlayerNet((int)world.getMapSpawn().x, (int)world.getMapSpawn().y, Entity.DEF_WIDTH, Entity.DEF_HEIGHT, address, port, ((PacketConnect) packet).getUsername());
			System.out.println("[" + address.getHostAddress() + ":" + port + "]" + ((PacketConnect) packet).getUsername() + " has connected");
			this.addConnection(player, (PacketConnect) packet);
			break;
		case DISCONNECT:
			packet = new PacketDisconnect(data);
			System.out.println("[" + address.getHostAddress() + ":" + port + "]" + ((PacketDisconnect) packet).getUsername() + " has disconnected");
			this.removeConnection((PacketDisconnect) packet);
			break;
		case ASKINFO:
			packet = new PacketServerInfo(serverOptions.mapName);
			((PacketServerInfo) packet).writeDataTo(this, ((PacketServerInfo) packet), address, port);
			break;
		case MOVE:
			packet = new PacketPlayerMove(data);
			handleMove((PacketPlayerMove) packet);
			break;
		case ROTATE:
			packet = new PacketRotate(data);
			handleMove((PacketRotate) packet);
			break;
		case PLAYERSHOOT:
			packet = new PacketPlayerShoot(data);
			handleShoot((PacketPlayerShoot) packet);
			
		default:
			break;
		}
	}


	public void addConnection(PlayerNet player, PacketConnect login) {
		boolean alreadyConnected = false;
		for(PlayerNet p : this.connectedPlayers) {
			if(p.getUsername().equals(login.getUsername())) {
				alreadyConnected = true;
				
				if(p.address == null) 
					p.address = player.address;
				
				if(p.port == -1)
					p.port = player.port;
			} else {
				sendData(login.getData(), p.address, p.port);
				login = new PacketConnect(p.getUsername());
				sendData(login.getData(), player.address, player.port);
				
				PacketPlayerMove move = new PacketPlayerMove(p.getUsername(), p.getPosition().x, p.getPosition().y);
				sendData(move.getData(), player.address, player.port);
				
				PacketRotate rotation = new PacketRotate(p.getUsername(), p.getAngle());
				sendData(rotation.getData(), player.address, player.port);
			}
		}
		
		if(!alreadyConnected) {
			this.connectedPlayers.add(player);
			for(Enemy e : worldHandler.getEnemies()) {
				PacketNewEnemy packetNewE = new PacketNewEnemy(e.getId(), e.getX(), e.getY(), e.getWidth(), e.getHeight());
				sendData(packetNewE.getData(), player.address, player.port);
			}
		}
	}
	
	private void removeConnection(PacketDisconnect packet) {
		if(playerExists(packet.getUsername())) {
			int index = getPlayerIndex(packet.getUsername());
			connectedPlayers.remove(index);
			packet.writeData(this);
		}
	}


	public void sendData(byte[] data, InetAddress address, int port) {
		try {
			DatagramPacket packet = new DatagramPacket(data, data.length, address, port);
			this.socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error " + e.toString(), "Fatal error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error binding port", "Fatal error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
			
		}
	}

	public void sendDataToAllClients(byte[] data) {
		for(PlayerNet p : connectedPlayers)
			this.sendData(data, p.address, p.port);
	}
	
	private void handleMove(PacketPlayerMove packet) {
		if(playerExists(packet.getUsername())) {
			int index = getPlayerIndex(packet.getUsername());
			this.connectedPlayers.get(index).getPosition().set(packet.getX(), packet.getY());
			packet.writeData(this);
		}
	}
	
	private void handleMove(PacketRotate packet) {
		if(playerExists(packet.getUsername())) {
			int index = getPlayerIndex(packet.getUsername());
			this.connectedPlayers.get(index).setAngle(packet.getAngle());
			packet.writeData(this);
		}
		
	}
	
	private void handleShoot(PacketPlayerShoot packet) {
		PacketNewBullet packetBullet = null;
		float angle = packet.getSpread();
		float finalAngle;
		
		if(packet.getProjectiles() == 1){
			this.addNewBullet(packet.getX(), packet.getY(), packet.getAngle(), packet.getDmg(), packetBullet);
		} else {
			for(int p = 0; p < packet.getProjectiles(); p++) {
				if(p==packet.getProjectiles()-1){
					finalAngle = packet.getAngle() - angle * ( packet.getProjectiles() - 1 ) / 2 + p * angle;
					this.addNewBullet(packet.getX(), packet.getY(), finalAngle, packet.getDmg() / packet.getProjectiles(), packetBullet);
				}
			}
		}

	}
	
	private void addNewBullet(float x, float y, float angle, int damage, PacketNewBullet packetBullet) {
		Bullet b = new Bullet(x, y, angle, damage);
		packetBullet = new PacketNewBullet(b.getId(), b.getX(), b.getY(), b.getAngle(), b.getDmg());
		packetBullet.writeData(this);
		worldHandler.newBullet(b);
	}
	
	public boolean playerExists(String username){
		if(username == null)
			return false;
		else
			for(PlayerNet p : connectedPlayers)
				if(p.getUsername().equals(username))
					return true;
		
		return false;
	}
	
	public int getPlayerIndex(String username) {
		int index = 0;
		for(PlayerNet p : connectedPlayers){
			if(p.getUsername().equals(username))
				break;
			index++;
		}
		return index;
	}


	public List<PlayerNet> getConnectedPlayers(){
		return this.connectedPlayers;
	}
}

