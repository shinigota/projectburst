/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.shinigota.ProjectBurst.ProjectBurst;
import com.shinigota.ProjectBurst.entities.Bullet;
import com.shinigota.ProjectBurst.entities.Enemy;
import com.shinigota.ProjectBurst.entities.Entity;
import com.shinigota.ProjectBurst.entities.net.PlayerNet;
import com.shinigota.ProjectBurst.packets.Packet;
import com.shinigota.ProjectBurst.packets.Packet.PacketTypes;
import com.shinigota.ProjectBurst.packets.PacketBulletMoved;
import com.shinigota.ProjectBurst.packets.PacketConnect;
import com.shinigota.ProjectBurst.packets.PacketDeleteEntity;
import com.shinigota.ProjectBurst.packets.PacketDisconnect;
import com.shinigota.ProjectBurst.packets.PacketEnemyDied;
import com.shinigota.ProjectBurst.packets.PacketEnemyMove;
import com.shinigota.ProjectBurst.packets.PacketEntityMove;
import com.shinigota.ProjectBurst.packets.PacketNewBullet;
import com.shinigota.ProjectBurst.packets.PacketNewEnemy;
import com.shinigota.ProjectBurst.packets.PacketPlayerMove;
import com.shinigota.ProjectBurst.packets.PacketRotate;
import com.shinigota.ProjectBurst.packets.PacketServerInfo;
import com.shinigota.ProjectBurst.world.World;

public class GameClient extends Thread {

	private InetAddress address;
	private DatagramSocket socket;
	private World world;
	private boolean ready;
	private ServerOptions serverInfos;
	public GameClient(World world, String address) {
		this.world = world;
		
		try {
			this.socket = new DatagramSocket();
			this.address = InetAddress.getByName(address);
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		this.serverInfos = null;
		this.ready = address.equals("localhost");		
		
	}
	
	public GameClient(String address) {
		try {
			this.socket = new DatagramSocket();
			this.address = InetAddress.getByName(address);
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		this.serverInfos = null;
		this.ready = address.equals("localhost");		
		
	}
	
	public void setWorld(World world) {
		this.world = world;
	}
	
	public void run() {
		
		while(true) {
			byte[] data = new byte[2048];
			DatagramPacket packet = new DatagramPacket(data, data.length);
			try {
				socket.receive(packet);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			this.parsePacket(packet.getData(), packet.getAddress(), packet.getPort());
		}
	}
	
	private void parsePacket(byte[] data, InetAddress address, int port) {
		String message = new String(data).trim();
		PacketTypes type = Packet.getType(message.substring(0, 2));
		Packet packet = null;
		
		switch(type){
		case INVALID:
			break;
		case CONNECT:
			packet = new PacketConnect(data);
			PlayerNet player = new PlayerNet((int) world.getMapSpawn().x, (int) world.getMapSpawn().y, Entity.DEF_WIDTH, Entity.DEF_HEIGHT, address, port, ((PacketConnect) packet).getUsername());
			System.out.println("[" + address.getHostAddress() + ":" + port + "]" +  ((PacketConnect) packet).getUsername() + " has joined");
			world.addPlayer(player);
			break;
		case DISCONNECT:
			packet = new PacketDisconnect(data);
			System.out.println("[" + address.getHostAddress() + ":" + port + "]" + ((PacketDisconnect) packet).getUsername() + " has left");
			world.removePlayer(((PacketDisconnect)packet).getUsername());
			break;
		case SERVERINFO:
			packet = new PacketServerInfo(data);
			this.serverInfos = new ServerOptions(((PacketServerInfo) packet).getMapName());
			this.ready = true;
			break;
		case MOVE:
			packet = new PacketPlayerMove(data);
			handleMove((PacketPlayerMove) packet);
			break;
		case ROTATE:
			packet = new PacketRotate(data);
			handleMove((PacketRotate) packet);
			break;
		case NEWENEMY:
			packet = new PacketNewEnemy(data);
			this.world.addEnemy(new Enemy(	((PacketNewEnemy) packet).getEntityId(), 
											((PacketNewEnemy) packet).getX(), 
											((PacketNewEnemy) packet).getY(), 
											((PacketNewEnemy) packet).getWidth(), 
											((PacketNewEnemy) packet).getHeight()));
			break;
		case ENEMYMOVE:
			packet = new PacketEnemyMove(data);
			handleMove((PacketEnemyMove) packet);
			break;
		case DELETEENTITY:
			packet = new PacketDeleteEntity(data);
			handleEntityErase((PacketDeleteEntity) packet);
			break;
		case NEWBULLET:
			packet = new PacketNewBullet(data);
			handleNewBullet((PacketNewBullet) packet);
			break;
		case BULLETMOVED:
			packet = new PacketBulletMoved(data);
			handleBulletMove((PacketBulletMoved) packet);
			break;
		case ENTITYMOVE:
			packet = new PacketEntityMove(data);
			handleEntityMove((PacketEntityMove) packet);
			break;
		default:
			break;
		}
	}


	private void handleEntityMove(PacketEntityMove packet) {
		this.world.updateEntity(packet.getId(), packet.getX(), packet.getY(), packet.getAngle());
	}

	public void sendData(byte[] data) {
		DatagramPacket packet = new DatagramPacket(data, data.length, address, ProjectBurst.DEF_PORT);
		try {
			socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void handleMove(PacketPlayerMove packet) {
		this.world.updatePlayerNet(packet.getUsername(), packet.getX(), packet.getY());
	}
	private void handleMove(PacketEnemyMove packet) {
		this.world.updateEnemy(packet.getId(), packet.getX(), packet.getY(), packet.getAngle());
	}
	private void handleMove(PacketRotate packet) {
		this.world.updatePlayerNet(packet.getUsername(), packet.getAngle());
	}
	
	private void handleBulletMove(PacketBulletMoved packet) {
		this.world.updateBullet(packet.getId(), packet.getX(), packet.getY());
	}

	private void handleNewBullet(PacketNewBullet packet) {
		Bullet b = new Bullet(packet.getId(), packet.getX(), packet.getY(), packet.getAngle(), packet.getDmg());
		this.world.addBullet(b);
	}

	private void handleEntityErase(PacketDeleteEntity packet) {
		this.world.deleteEntity(packet.getId());
	}
	
	public boolean isReady() {
		return this.ready;
	}
	
	public ServerOptions getServerOptions() {
		return this.serverInfos;
	}
	
	
}
