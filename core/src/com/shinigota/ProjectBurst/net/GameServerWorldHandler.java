/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.net;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CopyOnWriteArrayList;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.shinigota.ProjectBurst.ProjectBurst;
import com.shinigota.ProjectBurst.entities.Bullet;
import com.shinigota.ProjectBurst.entities.Enemy;
import com.shinigota.ProjectBurst.entities.Player;
import com.shinigota.ProjectBurst.map.EnemySpawn;
import com.shinigota.ProjectBurst.map.TiledMapCollider;
import com.shinigota.ProjectBurst.packets.PacketBulletMoved;
import com.shinigota.ProjectBurst.packets.PacketDeleteEntity;
import com.shinigota.ProjectBurst.packets.PacketEnemyDied;
import com.shinigota.ProjectBurst.packets.PacketEnemyMove;
import com.shinigota.ProjectBurst.packets.PacketEntityMove;
import com.shinigota.ProjectBurst.packets.PacketNewEnemy;
import com.shinigota.ProjectBurst.pathfinding.Path;
import com.shinigota.ProjectBurst.pathfinding.PathFinding;
import com.shinigota.ProjectBurst.pathfinding.Point;
import com.shinigota.ProjectBurst.world.World;

public class GameServerWorldHandler extends Thread {
	public static final float TICKRATE = 64, TICK_TIME = 1/TICKRATE;
	private int tick_interval = (int) (1000/TICKRATE);
	private GameServer server;
	private World world;
	private Array<EnemySpawn> spawns;
	private List<Bullet> bulletsToDelete;
	private List<Enemy> enemies;
	private List<Bullet> bullets;
	private ListIterator<Bullet> bIter;
	private Bullet b;
	private Enemy e, en;
	
	public GameServerWorldHandler(GameServer server, World world) {
		this.server = server;
		this.world = world;
		this.spawns = world.getEnemiesSpawns();
		this.enemies = new CopyOnWriteArrayList<Enemy>();
		this.bullets = new CopyOnWriteArrayList<Bullet>();
		this.bulletsToDelete = new ArrayList<Bullet>();
	}
	
	private void refreshEnemies(float delta) {
		for(Enemy e : enemies) {
			if(e.update(delta, (TiledMapTileLayer) world.getTiledMap().getLayers().get(0)) && e.isMoving)
				this.updateEntity(e.getId(), e.getX(), e.getY(), e.getAngle());	
				
			if(server.getConnectedPlayers().size() > 0) {
				Player player = e.getNearestPlayerNet(server.getConnectedPlayers());
				if(!e.hasPath() || (int) player.getX() / ProjectBurst.TILE_SIZE != e.getLastPathPoint().x / ProjectBurst.TILE_SIZE || (int) player.getY() / ProjectBurst.TILE_SIZE != e.getLastPathPoint().y / ProjectBurst.TILE_SIZE) {
					Path p = PathFinding.findPath((TiledMapTileLayer) world.getTiledMap().getLayers().get(0), new Point((int) e.getX(), (int) e.getY()), new Point((int) player.getX(), (int) player.getY()));
					e.setPath(p);
				}
			}
		}
	}
	
	private void refreshBullets(float delta) {
		bulletsToDelete.clear();
		en = null;
		bIter = bullets.listIterator();
		
		while(bIter.hasNext())
		{
			b = bIter.next();
			if(!b.hasCollided()) {
				b.update(delta, (TiledMapTileLayer) world.getTiledMap().getLayers().get(0));
				System.out.println("SERVER x:"+ b.getX() + " y:" + b.getY());
				en = TiledMapCollider.getEnemyCollide(b, enemies);
				if(en != null || b.hasCollided()) {
					if(en != null)
						this.deleteEntity(en.getId());
					this.deleteEntity(b.getId());
					enemies.remove(en);
					bulletsToDelete.add(b);
				}
			}
		}
		bullets.removeAll(bulletsToDelete);
		bulletsToDelete.clear();		
	}
	
	private void spawnEnemies(float delta) {
		enemies = world.getEnemies();
		for(EnemySpawn s : spawns) {
			if(s.canSpawn()) {
				e = s.spawn();
				PacketNewEnemy packetEnemy = new PacketNewEnemy(e.getId(), e.getX(), e.getY(), e.getWidth(), e.getHeight());
				packetEnemy.writeData(server);
			}
		}		
	}
	
	private void updateEntity(int id, float x, float y, float angle ) {
		PacketEntityMove entityMove = new PacketEntityMove(id, x, y, angle);
		server.sendDataToAllClients(entityMove.getData());
	}

	private void deleteEntity(int id) {
		PacketDeleteEntity packetDeleteBullet = new PacketDeleteEntity(id);
		server.sendDataToAllClients(packetDeleteBullet.getData());
	}
	
	public void run() {
		long lastRefresh = 0;
		long tmpRefresh;
		while(true) {
			
			while(TimeUtils.timeSinceMillis(lastRefresh) < TICK_TIME*1000);
			
			tmpRefresh = TimeUtils.millis();
			this.spawnEnemies(TimeUtils.timeSinceMillis(lastRefresh)/1000f);
			this.refreshEnemies(TimeUtils.timeSinceMillis(lastRefresh)/1000f);
			this.refreshBullets(TimeUtils.timeSinceMillis(lastRefresh)/1000f);
			lastRefresh = tmpRefresh;
		}
	}
	
	public List<Enemy> getEnemies() {
		return this.enemies;
	}

	public void newBullet(Bullet b) {
		this.bullets.add(b);
	}
	
	
}
