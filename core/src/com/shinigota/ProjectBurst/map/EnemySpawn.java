/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.map;

import com.badlogic.gdx.utils.TimeUtils;
import com.shinigota.ProjectBurst.entities.Enemy;
import com.shinigota.ProjectBurst.entities.MovableEntity;

public class EnemySpawn {
	private static final long SPAWN_DELAY = 2500;
	private float x, y;
	private long lastSpawn;
	
	public EnemySpawn(float f, float g) {
		this.x = f;
		this.y = g;
		this.lastSpawn = 0;
	}
	
	public boolean canSpawn() {
		return (TimeUtils.millis() - lastSpawn >= SPAWN_DELAY);
	}
	
	public Enemy spawn() {
		this.lastSpawn = TimeUtils.millis();
		return new Enemy(this.x, this.y, MovableEntity.DEF_WIDTH, MovableEntity.DEF_HEIGHT);
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	
	
	
}
