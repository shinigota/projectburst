/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.map;

import java.util.List;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.shinigota.ProjectBurst.entities.Enemy;
import com.shinigota.ProjectBurst.entities.Entity;
import com.shinigota.ProjectBurst.world.World;

public class TiledMapCollider {
	private static TiledMapTileLayer collisionLayer = null;
	public static float tileHeight, tileWidth;
	public static int width, height;
	public static World world;
	
	public static void init(TiledMapTileLayer collisionLayer, World world) {
		TiledMapCollider.collisionLayer = collisionLayer;
		TiledMapCollider.tileWidth = collisionLayer.getTileWidth();
		TiledMapCollider.tileHeight = collisionLayer.getTileHeight();
		TiledMapCollider.width = collisionLayer.getWidth();
		TiledMapCollider.height = collisionLayer.getHeight();
		TiledMapCollider.world = world;
	}
	
	public static boolean horizontalMovementCollides(Vector2 entityPosition, int entityWidth, int entityHeight, Vector2 entitySpeed) {
		if(entityPosition.x < 0)
			return true;
		if(entityPosition.x + entityWidth >= tileWidth * width)
			return true;
		
		boolean res = false;
		if(entitySpeed.x < 0) {
			res = collisionLayer.getCell((int) ((entityPosition.x ) / tileWidth), (int) ((entityPosition.y + entityHeight) / tileHeight)).getTile().getProperties().containsKey("blocked")
						|| collisionLayer.getCell((int) ((entityPosition.x) / tileWidth), (int) (entityPosition.y / tileHeight)).getTile().getProperties().containsKey("blocked");
			
		} else if (!res && entitySpeed.x > 0) {
			res = collisionLayer.getCell((int) ((entityPosition.x + entityWidth) / tileWidth), (int) ((entityPosition.y + entityHeight) / tileHeight)).getTile().getProperties().containsKey("blocked")
					|| collisionLayer.getCell((int) ((entityPosition.x +  entityWidth) / tileWidth), (int) (entityPosition.y / tileHeight)).getTile().getProperties().containsKey("blocked");
		}
		
		return res;
	}
	
	
	public static boolean verticalMovementCollides(Vector2 entityPosition, int entityWidth, int entityHeight, Vector2 entitySpeed) {
		if(entityPosition.y < 0)
			return true;
		if(entityPosition.y + entityHeight >= tileHeight * height)
			return true;
		
		boolean res = false;
		if(entitySpeed.y < 0) {
			res = collisionLayer.getCell((int) (entityPosition.x / tileWidth), (int) (entityPosition.y / tileHeight)).getTile().getProperties().containsKey("blocked")
						|| collisionLayer.getCell((int) ((entityPosition.x + entityWidth) / tileWidth), (int) (entityPosition.y / tileHeight)).getTile().getProperties().containsKey("blocked");
			
		} else if (!res && entitySpeed.y > 0) {
			res = collisionLayer.getCell((int) (entityPosition.x / tileWidth), (int) ((entityPosition.y + entityHeight) / tileHeight)).getTile().getProperties().containsKey("blocked")
					|| collisionLayer.getCell((int) ((entityPosition.x +  entityWidth) / tileWidth), (int) ((entityPosition.y + entityHeight) / tileHeight)).getTile().getProperties().containsKey("blocked");
		}
		
		return res;
	}
//	
//	public static boolean lineCollidesBlock(Player p, Line2D line, float angle) {
//		int startX, startY, endX, endY;
//		if(0 <= angle && angle <= 90 ) {
//
//			startX = (int) (p.getX() / tileWidth) - 1;
//			startY = (int) (p.getY() / tileHeight) - 1;
//			endX = (int) (tileWidth * width);
//			endY = (int) (tileHeight * height);
//		} else if(90 < angle && angle <= 180 ) {
//			startX = (int) (p.getX() / tileWidth) - 1;
//			startY = 0;
//			endX = (int) (tileWidth * width);
//			endY = (int) (p.getY() / tileHeight) + 1;
//		} else if(180 < angle && angle <= 270 ) {
//			startX = 0;
//			startY = 0;
//			endX = (int) (p.getX() / tileWidth) + 1;
//			endY = (int) (p.getY() / tileHeight) + 1;
//		} else {
//			startX = 0;
//			startY = (int) (p.getY() / tileHeight) - 1;
//			endX = (int) (p.getX() / tileWidth) + 1;
//			endY = (int) (tileHeight * height);
//		}
//		
//		
//	}
	
	public static boolean entityCollidesWitheEnemy(Entity et) {
		for(Enemy e : world.getEnemies()) 
			if(et.collides(e))
				return true;
		return false;
	}
	
	public static Enemy getEnemyCollide(Entity et, List<Enemy> enemies) {
		for(Enemy e : enemies) 
			if(et.collides(e))
				return e;
		return null;
	}
	
}
