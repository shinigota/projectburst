/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.pathfinding;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.shinigota.ProjectBurst.entities.Enemy;

public class PathFinding {
	private static TiledMapTileLayer map;
	private static ArrayList<Node> openedList, closedList;
	
	
	public static Path findPath(TiledMapTileLayer map, Point origin, Point dest) {
		Node destination = new Node((int) (dest.x / map.getTileWidth()), (int) (dest.y / map.getTileHeight()));
		PathFinding.map = map;
		openedList = new ArrayList<Node>();
		closedList = new ArrayList<Node>();
		ArrayList<Node> adjNodes = new ArrayList<Node>();
		Node current = new Node((int) (origin.x / map.getTileWidth()), (int) (origin.y / map.getTileHeight()));
		current.setCost_h(distance(current.x, current.y, destination.x, destination.y));

		openedList.add(current);
		
		while(openedList.size() > 0) {

			current = getSmallerCost_f();
			if(current.x == destination.x && current.y == destination.y){
				destination = current;
				break;
			}
				


			closedList.add(current);
			openedList.remove(current);
			adjNodes = getAdjacentsNodes(current, destination);
		
			for(Node n: adjNodes) {
				if(! isInClosedList(n) ) {
					
					if( isInOpenedList(n)) {
						
						float new_cost_f = current.getCost_g() + distance(current.x, current.y, n.x, n.y) +  + distance(current.x, current.y, destination.x, destination.y) ;
						if(new_cost_f < n.getCost_h()) {
							calculateNode(current, n, destination);
						}
					} else {
						calculateNode(current, n, destination);
						openedList.add(n);
					}
				}
			}
		}
		
		
		ArrayList<Point> path = new ArrayList<Point>();
		if(openedList.size() == 0)
			return new Path(path);
		Node lastNode = destination;
		while(lastNode != null) {
//			System.out.print(lastNode.x + ";" + lastNode.y + "      ");
			path.add(0, new Point((int) (lastNode.x * map.getTileWidth() + map.getTileWidth()/2 - Enemy.DEF_WIDTH/2), (int) (lastNode.y * map.getTileHeight() + map.getTileHeight()/2 - Enemy.DEF_HEIGHT/2)));
			lastNode = lastNode.getParent();
			
		}
//		System.out.println();
//		Collections.reverse(path);
		return new Path(path);
		
	}
	
	public static boolean isInOpenedList(Node node){
		for(Node n : openedList)
			if(n.x == node.x && n.y == node.y)
				return true;
		
		return false;
	}
	
	public static boolean isInClosedList(Node node){
		for(Node n : closedList)
			if(n.x == node.x && n.y == node.y)
				return true;
		
		return false;
	}
	
	public static float distance(float x1, float y1, float x2, float y2) {
		return (float) Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - 1));
	}
	
	public static void calculateNode(Node parent, Node node, Node dest) {
		node.setCost_g(parent.getCost_g() + distance(parent.x, parent.y, node.x, node.y));
		node.setCost_h(distance(node.x, node.y, dest.x, dest.y));
		node.setCost_f(node.getCost_g() + node.getCost_h());

		node.setParent(parent);
	}
	
	public static ArrayList<Node> getAdjacentsNodes(Node origin, Node dest) {

		ArrayList<Node> nodes = new ArrayList<Node>();
		ArrayList<Node> walkableNodes = new ArrayList<Node>();
		Node node, n2;
		Iterator<Node> nIter;
		boolean diagNode1ok, diagNode2ok;
		
		for(int y = origin.y - 1; y <= origin.y + 1; y ++) {
			for(int x = origin.x - 1; x <= origin.x + 1; x ++) {
				if(    	x >= 0 && y >= 0 && x <= map.getWidth() && y <= map.getHeight()
						&&	(x != origin.x || y != origin.y) 	  ) {
					
					node = new Node(x, y);
					if( map.getCell(x, y) == null || map.getCell(x, y).getTile().getProperties().containsKey("blocked")) {
						node.setWalkable(false);
					} else {
						node.setWalkable(true);
						node.setCost_h(distance(node.x, node.y, dest.x, dest.y));
					}
					nodes.add(node);

				}
			}	
		}
		
		for(Node n : nodes) {
		
			if(n.isWalkable()) {
				diagNode1ok = false;
				diagNode2ok = false;
				if(		(n.x == origin.x && n.y == origin.y - 1) || 
						(n.x == origin.x - 1 && n.y == origin.y)  || 
						(n.x == origin.x + 1 && n.y == origin.y) ||
						(n.x == origin.x && n.y == origin.y + 1)) {
					walkableNodes.add(n);
				} else if (n.x == origin.x -1 && n.y == origin.y - 1){
					
					nIter = nodes.iterator();
					while(nIter.hasNext() && !diagNode1ok && !diagNode2ok ) {
						n2 = nIter.next();
						
						if(n2.x == origin.x && n2.y == origin.y - 1 && n2.isWalkable())
							diagNode1ok = true;
						
						if(n2.x == origin.x - 1 && n2.y == origin.y && n2.isWalkable())
							diagNode2ok = true;
						
					}
					
					if(diagNode1ok && diagNode2ok)
						walkableNodes.add(n);
				} else if (n.x == origin.x -1 && n.y == origin.y + 1){
					
					nIter = nodes.iterator();
					while(nIter.hasNext() && !diagNode1ok && !diagNode2ok ) {
						n2 = nIter.next();
						
						if(n2.x == origin.x - 1 && n2.y == origin.y && n2.isWalkable())
							diagNode1ok = true;
						
						if(n2.x == origin.x  && n2.y == origin.y + 1 && n2.isWalkable())
							diagNode2ok = true;
						
					}
					
					if(diagNode1ok && diagNode2ok)
						walkableNodes.add(n);
				} else if (n.x == origin.x + 1 && n.y == origin.y + 1){
					
					nIter = nodes.iterator();
					while(nIter.hasNext() && !diagNode1ok && !diagNode2ok ) {
						n2 = nIter.next();
						
						if(n2.x == origin.x && n2.y == origin.y + 1 && n2.isWalkable())
							diagNode1ok = true;
						
						if(n.x == origin.x + 1 && n2.y == origin.y && n2.isWalkable())
							diagNode2ok = true;
						
					}
					
					if(diagNode1ok && diagNode2ok)
						walkableNodes.add(n);
				} else if (n.x == origin.x + 1 && n.y == origin.y - 1){
					
					nIter = nodes.iterator();
					while(nIter.hasNext() && !diagNode1ok && !diagNode2ok ) {
						n2 = nIter.next();
						
						if(n2.x == origin.x && n2.y == origin.y - 1 && n2.isWalkable())
							diagNode1ok = true;
						
						if(n2.x == origin.x + 1 && n2.y == origin.y && n2.isWalkable())
							diagNode2ok = true;
						
					}
					
					if(diagNode1ok && diagNode2ok)
						walkableNodes.add(n);
				}
			}
					
		}
		return walkableNodes;
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static Node getSmallerCost_f() {
		Node res = null;
		
		for(Node n: openedList) {
			if(res == null)
				res = n;
			else if(n.getCost_f() < res.getCost_f())
				res = n;
		}
		
		return res;
	}
//	public void findPath(Point origin, Point dest) {
//		origin.x /= map.getTileWidth();
//		origin.y /= map.getTileHeight();
//		dest.x /= map.getTileWidth();
//		dest.y /= map.getTileHeight();
//		
//		this.openedList = new ArrayList<Point>();
//		this.closedList = new ArrayList<Point>();
//		
//		
//	}
//	
//	
//	public ArrayList<Point> addAdjactensNodes(Point origin) {
//		ArrayList<Point> nodes = new ArrayList<Point>();
//		Point p;
//		
//		for(int y = origin.y - 1; y < origin.y + 1 ; origin.y ++) {
//			for(int x = origin.x - 1; x < origin.x; x++) {
//				if( ! ( ( 	x < 0 && y < 0 || x > map.getWidth() && y > map.getHeight() 
//						||	x == origin.x && y == origin.y 
//						|| 	map.getCell(x, y).getTile().getProperties().containsKey("blocked")) ) ) {
//					p = new Point(x, y);
//					if(! nodes.contains(p) )
//						nodes.add(p);
//				}
//			}
//		}
//		
//		return nodes;
//	}
}
