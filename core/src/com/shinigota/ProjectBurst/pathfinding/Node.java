/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.pathfinding;

public class Node extends Point{
	private float cost_g;
	private float cost_h;
	private float cost_f;
	private Node parent;
	private boolean walkable;
	
	public Node(int x, int y) {
		super(x, y);
		this.cost_g = 0;
		this.cost_h = 0;
		this.cost_f = 0;
		this.parent = null;
	}

	public float getCost_g() {
		return cost_g;
	}

	public void setCost_g(float cost_g) {
		this.cost_g = cost_g;
	}

	public float getCost_h() {
		return cost_h;
	}

	public void setCost_h(float cost_h) {
		this.cost_h = cost_h;
	}

	public float getCost_f() {
		return cost_f;
	}

	public void setCost_f(float cost_f) {
		this.cost_f = cost_f;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == null)
			return false;
		if(o.getClass() != this.getClass())
			return false;
		
		return ((Point) o).x == this.x && ((Point) o).y == this.y;
	}

	public boolean isWalkable() {
		return walkable;
	}

	public void setWalkable(boolean walkable) {
		this.walkable = walkable;
	}

}
