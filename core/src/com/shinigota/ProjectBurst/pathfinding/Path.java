/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.pathfinding;

import java.util.ArrayList;
import java.util.Iterator;

public class Path {
	public ArrayList<Point> points;
	private Iterator<Point> pIter;
	private Point p;
	
	public Path(ArrayList<Point> points) {
		this.points = points;
		this.pIter = points.iterator();
	}
	
	public boolean hasNext() {
		return this.pIter.hasNext();
	}
	
	public Point nextPoint() {
		if(this.hasNext())
			this.p = pIter.next();
		return p;
	}
	
	public Point getCurrentPoint() {
		return this.p;
	}
	
	public Point getLastPoint() {
		if(points.size() > 0)
			return points.get(points.size()-1);
		return null;
	}
}
