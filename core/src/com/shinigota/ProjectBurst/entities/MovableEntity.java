/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.entities;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;

public abstract class MovableEntity extends Entity {
	
	
	public enum Direction {
		TOP, RIGHT, BOTTOM, LEFT, NONE
	}
	
	private Direction direction, previousDirection;
	public boolean isMoving;
	public final float SPEED;
	public Vector2 speed, tmpSpeed;
	public float rotation;
	
	public MovableEntity(int id, float x, float y, int width, int height, float speed) {
		super(id, x, y, width, height);
		this.isMoving = false;
		this.SPEED = speed;
		this.previousDirection = Direction.NONE;
		this.rotation = 0;
		this.speed = Vector2.Zero;
		
	}
	
	public MovableEntity(float x, float y, int width, int height, float speed) {
		super(x, y, width, height);
		this.isMoving = false;
		this.SPEED = speed;
		this.previousDirection = Direction.NONE;
		this.rotation = 0;
		this.speed = Vector2.Zero;
		
	}
	
	public MovableEntity(int id, float x, float y, int width, int height, float speed, float angle) {
		super(id, x, y, width, height);
		this.isMoving = false;
		this.SPEED = speed;
		this.previousDirection = Direction.NONE;
		this.rotation = 0;
		this.speed = Vector2.Zero;
		this.angle = angle;
	}
	
	public MovableEntity(float x, float y, int width, int height, float speed, float angle) {
		super(x, y, width, height);
		this.isMoving = false;
		this.SPEED = speed;
		this.previousDirection = Direction.NONE;
		this.rotation = 0;
		this.speed = Vector2.Zero;
		this.angle = angle;
	}
	
	public abstract boolean update(float delta, TiledMapTileLayer collisionLayer);
	
	public void moveUp(){
		this.previousDirection = this.direction;
		this.direction = Direction.TOP;
		this.isMoving = true;
	}
	
	public void moveDown(){
		this.previousDirection = this.direction;
		this.direction = Direction.BOTTOM;
		this.isMoving = true;
	}
	
	public void moveRight(){
		this.previousDirection = this.direction;
		this.direction = Direction.RIGHT;
		this.isMoving = true;
	}
	
	public void moveLeft(){
		this.previousDirection = this.direction;
		this.direction = Direction.LEFT;
		this.isMoving = true;
	}
	
//	protected boolean horizontalUpdate
	public void stopMoving(){
		this.isMoving = false;
	}

	public Direction getDirection() {
		return direction;
	}
	
	public Direction getPreviousDirection() {
		return previousDirection;
	}	
	
	public float getSpeed(){
		return this.SPEED;
	}
}
