/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.entities;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.shinigota.ProjectBurst.pathfinding.Path;
import com.shinigota.ProjectBurst.pathfinding.Point;

/**
 * @author bbarbe
 *
 */
public class Enemy extends LivingEntity{
	public static final int TYPE = 2;
	public final static float DEF_SPEED = 30f;
	private Path path;
	private Point target;
	
	/**
	 * Creates an Enemy from specific params
	 * @param id Entity's ID, must be unique
	 * @param x horizontal position
	 * @param y vertical position
	 * @param width 
	 * @param height
	 */
	public Enemy(int id, float x, float y, int width, int height) {
		super(id, x, y, width, height, DEF_SPEED);
		this.speed = new Vector2(1, 0);
		this.path = null;
	}
	
	/**
	 * Creates an Enemy from specific params, automatically generates ID
	 * @param x horizontal position
	 * @param y vertical position
	 * @param width 
	 * @param height
	 */
	public Enemy(float x, float y, int width, int height) {
		super(x, y, width, height, DEF_SPEED);
		this.speed = new Vector2(1, 0);
		this.path = null;
	}

	/**
	 * Changes Enemy's path it has to follow
	 * @param p new Path
	 */
	public void setPath(Path p) {
		this.path = p;
		this.goToNextCell();
		this.goToNextCell();
		if(this.target != null)
			this.isMoving = true;
	}

	/**
	 * Move Enemy to the next path's cell
	 */
	public void goToNextCell() {
		if(path != null) {
			this.target = path.nextPoint();
			this.isMoving = true;
		}
	}

	/**
	 * Check if destination cell has be reached
	 * @return Destination cell is reached or not
	 */
	public boolean cellReached() {
		if (this.target == null || this.position.x + this.width/2 == target.x && this.position.y +this. height/2== target.y ) {
			this.isMoving = false;
			return true;
		}
		
		return false;
	}
	
	
	/* (non-Javadoc)
	 * @see com.shinigota.ProjectBurst.entities.MovableEntity#update(float, com.badlogic.gdx.maps.tiled.TiledMapTileLayer)
	 */
	@Override
	public boolean update(float delta, TiledMapTileLayer collisionLayer){
		boolean sendUpdate = false;
		if(this.target == null)
			return false;
		if(isMoving) {
			if(this.cellReached()) {
				this.goToNextCell();
				sendUpdate = true;
			}
			if(this.target != null) {
				this.moveToNextCell(delta);
			}
		} else {
			if(this.cellReached()) {
				this.goToNextCell();
				sendUpdate = true;
			}
			this.speed = Vector2.Zero;
		}
		
		return sendUpdate;
	}
	
	/**
	 * Move the Enemy to the destination frame
	 * @param delta time between frames
	 */
	private void moveToNextCell(float delta) {
		this.angle = new Vector2(target.x, target.y).sub(position).angle();
		this.speed.nor().scl(DEF_SPEED).scl(delta).setAngle(this.angle);
		if(this.position.dst(this.position.cpy().add(this.speed)) > this.position.dst(target.x, target.y) ){
			this.position.set(target.x, target.y);
			this.goToNextCell();
		} else {
			this.position.add(this.speed);
		}
	}
	
	/**
	 * @return Enemy has a path nor not
	 */
	public boolean hasPath() {
		return path != null;
	}
	
	/**
	 * @return Last path cell's position
	 */
	public Point getLastPathPoint() {
		return this.path.getLastPoint();
	}
}
