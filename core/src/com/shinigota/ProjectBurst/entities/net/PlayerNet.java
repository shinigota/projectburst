/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.entities.net;

import java.net.InetAddress;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.shinigota.ProjectBurst.entities.Player;
import com.shinigota.ProjectBurst.packets.PacketPlayerMove;
import com.shinigota.ProjectBurst.packets.PacketPlayerShoot;
import com.shinigota.ProjectBurst.screens.GameScreen;

public class PlayerNet extends Player {

	public InetAddress address;
	public int port;
	
	private String username;
	
	
	public PlayerNet(int x, int y, int width, int height, InetAddress address, int port, String username) {
		super(x, y, width, height, Player.SPEED);
		this.address = address;
		this.port = port;
		this.username = username;
		
	}

	public String getUsername() {
		return username;
	}
	
	@Override
	public boolean update(float delta, TiledMapTileLayer collisionLayer){
		super.update(delta,collisionLayer);
		if(this.isMoving) {
			PacketPlayerMove packet = new PacketPlayerMove(this.username, this.getX(), this.getY());
			packet.writeData(GameScreen.game.client);		
		}
		return true;
	}

	public void shoot() {
		PacketPlayerShoot packet = new PacketPlayerShoot(this.username, this.getCenter().x, this.getCenter().y, this.getAngle() + 90, this.getEquipedWeapon().getDmg(), this.getEquipedWeapon().getProjectiles(), this.getEquipedWeapon().getSpead());
		packet.writeData(GameScreen.game.client);
	}

}
