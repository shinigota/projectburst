/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.entities;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.badlogic.gdx.math.Vector2;
import com.shinigota.ProjectBurst.entities.net.PlayerNet;

public class Entity {
	public static final int DEF_WIDTH = 30, DEF_HEIGHT = 30;
	public static AtomicInteger idGen = new AtomicInteger();
	
	private int id;
	protected int width, height;
	protected Vector2 position;
	protected float angle;
	
	public Entity(int id, float x, float y, int width, int height) {
		super();
		this.position = new Vector2(x, y);
		this.width = width;
		this.height = height;
		this.angle = 0;
		this.id = id;
	}
	
	public Entity(float x, float y, int width, int height) {
		super();
		this.position = new Vector2(x, y);
		this.width = width;
		this.height = height;
		this.angle = 0;
		this.id = idGen.getAndIncrement();
	}

	public Vector2 getPosition() {
		return position;
	}


	public float getX() {
		return position.x;
	}

	public float getY() {
		return position.y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public float getAngle() {
		return angle;
	}

	public void setAngle(float angle) {
		this.angle = angle;
	}
	
	public int getId() {
		return this.id;
	}
	
	public Player getNearestPlayer(List<Player> list) {
		if(list.size() <= 0) 
			return null;
		
		Player nearest = list.get(0);
		for(Player p : list) {
			if(this.distance(p.getPosition()) < this.distance(nearest.getPosition()))
				nearest = p;
		}
		
		return nearest;
	}
	
	public PlayerNet getNearestPlayerNet(List<PlayerNet> list) {
		if(list.size() <= 0) 
			return null;
		
		PlayerNet nearest = list.get(0);
		for(PlayerNet p : list) {
			if(this.distance(p.getPosition()) < this.distance(nearest.getPosition()))
				nearest = p;
		}
		
		return nearest;
	}
	
	public float distance(Vector2 pos) {
		return this.position.dst(pos);
	}
	
	public Vector2 getCenter() {
		return new Vector2(position.x + width/2, position.y + height/2);
	}
	
	public boolean collides(Entity e) {
		return !(	this.position.x > e.position.x + e.width || 
					this.position.x + this.width < e.position.x || 
					this.position.y + this.height < e.position.y - e.height || 
					this.position.y > e.position.y + e.height);
	}
	
}
