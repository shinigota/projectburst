/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.entities;

import java.util.List;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.shinigota.ProjectBurst.map.TiledMapCollider;

/**
 * @author bbarbe
 *
 */
public class Bullet extends MovableEntity {
	public static final int DEF_WIDTH = 2, DEF_HEIGHT = 2, TYPE = 1;
	public static final float DEF_SPEED = 300f;
	private boolean hasCollided;
	private int dmg;
	
	/**
	 * Creates a bullet from specific params
	 * @param id Entity's ID, must be unique
	 * @param x horizontal position
	 * @param y vertical position
	 * @param angle direction of the bullet
	 * @param dmg damages dealt by the bullet on collision
	 */
	public Bullet(int id, float x, float y, float angle, int dmg) {
		super(id, x, y, DEF_WIDTH, DEF_HEIGHT, DEF_SPEED, angle);
		this.hasCollided = false;
		this.dmg = dmg;
		this.speed = new Vector2(1, 0);
	}

	/**
	 * Creates a bullet from specific params, automatically generates the ID
	 * @param x horizontal position
	 * @param y vertical position
	 * @param angle direction of the bullet
	 * @param dmg damages dealt by the bullet on collision
	 */
	public Bullet(float x, float y, float angle, int dmg) {
		super(x, y, DEF_WIDTH, DEF_HEIGHT, DEF_SPEED, angle);
		this.hasCollided = false;
		this.dmg = dmg;
		this.speed = new Vector2(1, 0);
	}

	/* 
	 * Updates the current bullet's position
	 * @see com.shinigota.ProjectBurst.entities.MovableEntity#update(float, com.badlogic.gdx.maps.tiled.TiledMapTileLayer)
	 */
	public boolean update(float delta, TiledMapTileLayer collisionLayer) {
		Vector2 oldPos = this.position.cpy();
		tmpSpeed = speed.nor().scl(DEF_SPEED).scl(delta).setAngle(angle).cpy();
		
		this.position.x += tmpSpeed.x;
		if(TiledMapCollider.horizontalMovementCollides(this.position, this.width, this.height, this.speed)) {
			position.x = oldPos.x;
			this.hasCollided = true;
		}
		
		this.position.y += tmpSpeed.y;
		if(TiledMapCollider.verticalMovementCollides(this.position, this.width, this.height, this.speed)) {
			position.y = oldPos.y;
			this.hasCollided = true;
		}
		
		return true;
	}
	
	/**
	 * Returns the enemy that collides with the current bullet
	 * @param enemies
	 * @return the enemy colliding, null if no collision is happening
	 */
	public Enemy hitEnemy(List<Enemy> enemies) {
		Enemy e = null;
		if((e = TiledMapCollider.getEnemyCollide(this, enemies)) != null)
			this.hasCollided = true;
		return e;
	}

	/**
	 * Gets collision state of the bullet
	 * @return true if the bullet has collided, otherwise false
	 */
	public boolean hasCollided() {
		return hasCollided;
	}

	/**
	 * Changes state of collision of the bullet
	 * @param collided the new collision state
	 */
	public void setCollided(boolean collided) {
		this.hasCollided = collided;
	}

	/**
	 * Gets the amount of damage the bullet deals
	 * @return the amount of damage the bullet deals
	 */
	public int getDmg() {
		return dmg;
	}

}
