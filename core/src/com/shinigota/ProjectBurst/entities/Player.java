/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.entities;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.shinigota.ProjectBurst.map.TiledMapCollider;

public abstract class Player extends LivingEntity {

	public static final float SPEED = 70f;
	public Weapon equipedWeapon;
	
	public enum Weapon {
		MELEE(5, 1, 0), SAWED_OFF(50, 5, 5);
		
		private int dmg, projectiles, spread;

		private Weapon(int dmg, int projectiles, int spread) {
			this.dmg = dmg;
			this.projectiles= projectiles;
			this.spread = spread;
		}

		public int getDmg() {
			return dmg;
		}

		public int getProjectiles() {
			return projectiles;
		}
		
		public int getSpead() {
			return spread;
		}
	}
	
	public Player(int id, int x, int y, int width, int height) {
		super(id, x, y, width, height, SPEED);
		this.equipedWeapon = Weapon.SAWED_OFF;
	}
	
	public Player(int id, int x, int y, int width, int height, float speed) {
		super(id, x, y, width, height, speed);
		this.equipedWeapon = Weapon.SAWED_OFF;
	}
	
	public Player(int x, int y, int width, int height) {
		super(x, y, width, height, SPEED);
		this.equipedWeapon = Weapon.SAWED_OFF;
	}
	
	public Player(int x, int y, int width, int height, float speed) {
		super(x, y, width, height, speed);
		this.equipedWeapon = Weapon.SAWED_OFF;
	}
	
	public Weapon getEquipedWeapon() {
		return this.equipedWeapon;
	}

	@Override
	public boolean update(float delta, TiledMapTileLayer collisionLayer) {
		if(isMoving) {
			Vector2 oldPos = this.position.cpy();
			tmpSpeed = speed.cpy();
		
			this.position.x += tmpSpeed.x;
			if(TiledMapCollider.horizontalMovementCollides(this.position, this.width, this.height, this.speed)) {
				position.x = oldPos.x;
			}
			
			
			this.position.y += tmpSpeed.y;
			if(TiledMapCollider.verticalMovementCollides(this.position, this.width, this.height, this.speed)) {
				position.y = oldPos.y;
			}
		}
		
		return true;
	}
}
