/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.entities;

public abstract class LivingEntity extends MovableEntity{
	public static final int START_HEALTH = 100;
	private int health;
	
	public LivingEntity(int id, float x, float y, int width, int height, float speed) {
		super(id, x, y, width, height, speed);
		// TODO Auto-generated constructor stub
	}
	
	public LivingEntity(float x, float y, int width, int height, float speed) {
		super(x, y, width, height, speed);
		// TODO Auto-generated constructor stub
	}
	
	public int getHealth() {
		return this.health;
	}
	public boolean updateHealth(int deltaHp) {
		this.health += deltaHp;
		
		return(health <= 0);
	}


}
