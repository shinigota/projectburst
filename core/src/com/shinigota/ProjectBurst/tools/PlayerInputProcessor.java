/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.shinigota.ProjectBurst.entities.Entity;
import com.shinigota.ProjectBurst.entities.net.PlayerNet;
import com.shinigota.ProjectBurst.packets.PacketRotate;
import com.shinigota.ProjectBurst.screens.GameScreen;

public class PlayerInputProcessor implements InputProcessor{

	private PlayerNet player;
	private OrthographicCamera cam;
	private float delta;
	
	public PlayerInputProcessor(PlayerNet player, OrthographicCamera cam) {
		this.player = player;
		this.cam = cam;
	}

	@Override
	public boolean keyDown(int keycode) {
		delta = Gdx.graphics.getDeltaTime();
		if(keycode == Keys.Z || keycode == Keys.S || keycode == Keys.D || keycode == Keys.Q) player.isMoving = true;
		if(keycode == Keys.Z) player.speed.y = player.getSpeed() * delta;
		if(keycode == Keys.S) player.speed.y = -player.getSpeed() * delta;
		if(keycode == Keys.D) player.speed.x = player.getSpeed() * delta;
		if(keycode == Keys.Q) player.speed.x = -player.getSpeed() * delta;
		
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		delta = Gdx.graphics.getDeltaTime();
		if(!Gdx.input.isKeyPressed(Keys.Z) && !Gdx.input.isKeyPressed(Keys.S) &&!Gdx.input.isKeyPressed(Keys.D) &&!Gdx.input.isKeyPressed(Keys.Q))
			player.stopMoving();
		
		if(keycode == Keys.Z)
			if(Gdx.input.isKeyPressed(Keys.S))
				player.speed.y = -player.getSpeed() * delta;
			else
				player.speed.y = 0;
		else if(keycode == Keys.S)
			if(Gdx.input.isKeyPressed(Keys.Z))
				player.speed.y = player.getSpeed() * delta;
			else
				player.speed.y = 0;
		else if(keycode == Keys.D)
			if(Gdx.input.isKeyPressed(Keys.Q))
				player.speed.x = -player.getSpeed() * delta;
			else
				player.speed.x = 0;
		if(keycode == Keys.Q)
			if(Gdx.input.isKeyPressed(Keys.D))
				player.speed.x = player.getSpeed() * delta;
			else
				player.speed.x = 0;
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if(button == Buttons.LEFT) {
			player.shoot();
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		Vector3 pos3d = new Vector3(screenX, screenY, 0);
		Vector3 pos3dUnprojected = cam.unproject(pos3d);
		Vector2 mousePos = new Vector2(pos3dUnprojected.x, pos3dUnprojected.y);
		
		Vector2 relativePos = mousePos.cpy().sub(player.getPosition()).sub(Entity.DEF_WIDTH/2, Entity.DEF_HEIGHT/2);
		player.setAngle((relativePos.angle() - 90));
		PacketRotate packet = new PacketRotate(player.getUsername(), player.getAngle());
		packet.writeData(GameScreen.game.client);
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
