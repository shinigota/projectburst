/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.tools;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class TextInput extends Label{

	private int index;
	private boolean focused;
	private String text;
	
	public TextInput(CharSequence text, Skin skin) {
		super(text, skin, "input_label");
		this.text = text.toString();
		setWidth(200);
		this.index = text.length();
		this.focused = false;
	}

	public boolean isFocused() {
		return focused;
	}
	
	public void focus() {
		focused = true;
		index = this.getText().length;
	}
	
	public void stopFocus() {
		focused = false;
		index = 0;
	}
	
	public void nextChar() {
		if(getText() != null)
			if(index < getText().length)
				index++;
	}
	
	public void previousChar() {
		if(getText() != null)
			if(index <= getText().length && index > 0)
				index--;
	}
	
	public void deleteChar() {
		if(getText() != null)
			if(index <= getText().length && index > 0) {
				text = text.substring(0, index - 1) + text.substring(index);
				setText(text);
				index--;
			}
				
	}
	
	public void addChar(char c) {
		StringBuilder builder = new StringBuilder(text);
		text = builder.insert(index, c).toString();
		setText(text);
		index++;
	}
	
	public int getCursor() {
		return index;
	}
	
	public boolean contains(int x, int y) {
		return (this.getX() <= x && x <= this.getX() + this.getWidth() && this.getY() <= y && y <= this.getY() + this.getHeight());
	}
}

