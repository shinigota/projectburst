/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.world;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.shinigota.ProjectBurst.entities.Bullet;
import com.shinigota.ProjectBurst.entities.Enemy;
import com.shinigota.ProjectBurst.entities.Entity;
import com.shinigota.ProjectBurst.entities.MovableEntity;
import com.shinigota.ProjectBurst.entities.Player;
import com.shinigota.ProjectBurst.entities.net.PlayerNet;

public class WorldRenderer{

	private OrthographicCamera cam;
	private ShapeRenderer sr;
	private SpriteBatch batch;
	
	private World world;
	
	private TextureAtlas gignAtlas;
	private Sprite gign;
	
	private TextureAtlas enemiesAtlas;
	private Sprite zombie;
	private Texture shadowRight, shadowRightTrim, shadowBot, shadowBotTrim, bulletTexture;
	
	private BitmapFont usernameFont, uiFont;
	
	private TiledMapRenderer mapRenderer;
	private TiledMapTileLayer mapLayer;
	
	private GlyphLayout usernameLayout, uiLayout;
	
	public WorldRenderer(World world) {
		this.world = world;
		this.cam = new OrthographicCamera(800,600);
		this.sr = new ShapeRenderer();
		this.batch = new SpriteBatch();
		
		this.gignAtlas = new TextureAtlas(Gdx.files.internal("textures/humans/gign.pack"));
		AtlasRegion region = this.gignAtlas.findRegion("gign_sawed_off");
		this.gign = new Sprite(region);
		this.gign.setOrigin(Entity.DEF_WIDTH/2, Entity.DEF_HEIGHT/2);
		this.gign.setBounds(0, 0, Entity.DEF_WIDTH, Entity.DEF_HEIGHT);
		
		this.enemiesAtlas = new TextureAtlas(Gdx.files.internal("textures/enemies/enemies.pack"));
		AtlasRegion region2 = this.enemiesAtlas.findRegion("zombie_claws");
		this.zombie = new Sprite(region2);
		this.zombie.setOrigin(Entity.DEF_WIDTH/2, Entity.DEF_HEIGHT/2);
		this.zombie.setBounds(0, 0, Entity.DEF_WIDTH, Entity.DEF_HEIGHT);
		
		this.shadowRight = new Texture(Gdx.files.internal("textures/map_effects/shadow_right.png"));
		this.shadowBot = new Texture(Gdx.files.internal("textures/map_effects/shadow_bot.png"));
		this.shadowRightTrim = new Texture(Gdx.files.internal("textures/map_effects/shadow_right_trimmed.png"));
		this.shadowBotTrim = new Texture(Gdx.files.internal("textures/map_effects/shadow_bot_trimmed.png"));
		
		this.bulletTexture = new Texture(Gdx.files.internal("textures/bullets/bullet.png"));
		
		this.mapRenderer = new OrthogonalTiledMapRenderer(world.getTiledMap());
		this.mapLayer = (TiledMapTileLayer) world.getTiledMap().getLayers().get(0);
		
		this.usernameFont = new BitmapFont(Gdx.files.internal("skins/fonts/font1.fnt"));
		this.usernameFont.setColor(Color.GREEN);
		this.uiFont = this.usernameFont;
	}


	public void render(float delta) {
		List<Entity> entities = world.getAllEntities();
		cam.position.set(world.getLocalPlayer().getX() + world.getLocalPlayer().getWidth()/2, world.getLocalPlayer().getY() + world.getLocalPlayer().getHeight()/2, 0);
		cam.update();
		
		mapRenderer.setView(cam);
		mapRenderer.render();
		sr.setProjectionMatrix(cam.combined);
		sr.begin(ShapeType.Line);
		for(Entity e : entities) {
			if(e instanceof PlayerNet) {
				sr.rect(e.getX(), e.getY(), e.getWidth(), e.getHeight());
			}
		}
		sr.end();
		
		batch.setProjectionMatrix(cam.combined);
		batch.begin();
		this.drawShadow();
		for(Entity e : entities) {
			if (e instanceof Enemy) {
				zombie.setBounds(e.getX(), e.getY(), e.getWidth(), e.getHeight());
				zombie.setRotation(((MovableEntity)e).getAngle() - 90);
				zombie.draw(batch);
			} else if (e instanceof Bullet) {
				batch.draw(bulletTexture, e.getX(), e.getY(), e.getWidth(), e.getHeight());
			} else if(e instanceof Player) {
				gign.setBounds(e.getX(), e.getY(), e.getWidth(), e.getHeight());
				gign.setRotation(((MovableEntity)e).getAngle());
				gign.draw(batch);
				usernameLayout = new GlyphLayout(usernameFont, ((PlayerNet) e).getUsername());
				usernameFont.draw(batch, (((PlayerNet) e).getUsername()), e.getX() +e.getWidth()/2- usernameLayout.width / 2 , e.getY());
			}
		}
		uiLayout = new GlyphLayout(uiFont, "+ " + world.getLocalPlayer().getHealth());
		uiFont.draw(batch, "+ " + world.getLocalPlayer().getHealth(), world.getLocalPlayer().getCenter().x - cam.viewportWidth/2 + 5, world.getLocalPlayer().getCenter().y - cam.viewportHeight/2 + uiLayout.height + 5);
		batch.end();
	}


	private void drawShadow() {
		for(int y = 0; y < mapLayer.getHeight(); y++) {
			for(int x = 0; x < mapLayer.getWidth(); x++) {
				if(mapLayer.getCell(x, y).getTile().getProperties().containsKey("blocked")) {
					
					if(mapLayer.getCell(x, y - 1) != null && !mapLayer.getCell(x, y - 1).getTile().getProperties().containsKey("blocked")) {
						if(mapLayer.getCell(x + 1, y - 1) == null || mapLayer.getCell(x + 1, y - 1).getTile().getProperties().containsKey("blocked"))
							batch.draw(shadowBotTrim, x * mapLayer.getTileWidth(), y * mapLayer.getTileHeight() - shadowBotTrim.getHeight());
						else
							batch.draw(shadowBot, x * mapLayer.getTileWidth(), y * mapLayer.getTileHeight() - shadowBot.getHeight());
					}
					if(mapLayer.getCell(x + 1, y) != null && !mapLayer.getCell(x + 1, y).getTile().getProperties().containsKey("blocked")) {
						if(mapLayer.getCell(x + 1, y - 1) == null || mapLayer.getCell(x + 1, y - 1).getTile().getProperties().containsKey("blocked"))
							batch.draw(shadowRightTrim, (x+1) * mapLayer.getTileWidth(), y * mapLayer.getTileHeight());
						else
							batch.draw(shadowRight, (x+1) * mapLayer.getTileWidth(), y * mapLayer.getTileHeight() + (shadowRight.getHeight() - mapLayer.getHeight()) + 1 );
					}
				}
			}
		}
	}


	public void dispose() {
		sr.dispose();
	}

	public OrthographicCamera getCam() {
		return cam;
	}

}
