/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.world;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.shinigota.ProjectBurst.entities.Bullet;
import com.shinigota.ProjectBurst.entities.Enemy;
import com.shinigota.ProjectBurst.entities.Entity;
import com.shinigota.ProjectBurst.entities.MovableEntity;
import com.shinigota.ProjectBurst.entities.Player;
import com.shinigota.ProjectBurst.entities.net.PlayerNet;
import com.shinigota.ProjectBurst.map.EnemySpawn;
import com.shinigota.ProjectBurst.net.GameServerWorldHandler;

public class World {
	private PlayerNet localPlayer;
	private List<Entity> entities;
	private List<Enemy> enemies;
	private List<Player> players;
	private List<Bullet> bullets;

	private List<Enemy> enemiesToDelete;
	
	private TiledMap map;
	private long lastRefresh = 0, tmpRefresh;
	public World(String mapName) {
		this.entities = new CopyOnWriteArrayList<Entity>();
		this.enemies = new CopyOnWriteArrayList<Enemy>();
		this.players = new CopyOnWriteArrayList<Player>();
		this.bullets = new CopyOnWriteArrayList<Bullet>();
		
		this.enemiesToDelete = new ArrayList<Enemy>();
		
		this.localPlayer = null;
		this.map = new TmxMapLoader().load(mapName);
	}

	public void update(float delta) {

		
		while(TimeUtils.timeSinceMillis(lastRefresh) < GameServerWorldHandler.TICK_TIME*1000);
		
		tmpRefresh = TimeUtils.millis();
		
		
		for(Enemy e : this.enemies)
			e.update(TimeUtils.timeSinceMillis(lastRefresh)/1000f, (TiledMapTileLayer) map.getLayers().get(0));
		for(Player p : this.players)
			p.update(TimeUtils.timeSinceMillis(lastRefresh)/1000f, (TiledMapTileLayer) map.getLayers().get(0));
		
		for(Bullet b : this.bullets) {
			b.update(TimeUtils.timeSinceMillis(lastRefresh)/1000f, (TiledMapTileLayer) map.getLayers().get(0));
			System.out.println("CLIENT x:"+ b.getX() + " y:" + b.getY());
		}
		
		this.enemies.removeAll(enemiesToDelete);
		this.enemiesToDelete.clear();
		
		lastRefresh = tmpRefresh;
	}

	public void addPlayer(PlayerNet playerNet) {
		this.players.add(playerNet);
	}

	public void removePlayer(String username) {
		players.remove(getPlayerIndex(username));
	}

	public List<Entity> getEntities() {
		return entities;
	}
	
	public List<Entity> getAllEntities() {
		List<Entity> entities = new CopyOnWriteArrayList<Entity>();
		entities.addAll(players);
		entities.addAll(enemies);
		entities.addAll(bullets);
		
		return entities;
	}
	
	public List<Player> getPlayers() {
		return players;
	}
	
	public List<Enemy> getEnemies() {
		return enemies;
	}
	
	public List<Bullet> getBullets() {
		return bullets;
	}

	public void addLocalPlayer(PlayerNet player) {
		this.players.add(player);
		this.localPlayer = player;		
	}
	
	public void addEntity(Entity e) {
		this.entities.add(e);
	}
	
	public void addEnemy(Enemy e) {
		this.enemies.add(e);
	}

	public PlayerNet getLocalPlayer() {
		return localPlayer;
	}

	public void updatePlayerNet(String username, float x, float y) {
		if(this.playerExists(username)) {
			PlayerNet p = (PlayerNet) this.players.get(this.getPlayerIndex(username));
			p.getPosition().set(x, y);
		}
	}

	public void updatePlayerNet(String username, float angle) {
		if(this.playerExists(username)) {
			PlayerNet p = (PlayerNet) this.players.get(this.getPlayerIndex(username));
			p.setAngle(angle);
		}
	}
	
	public void updateEnemy(int id, float x, float y, float angle) {
		Enemy e = this.enemies.get(this.getEnemyIndex(id));
		e.getPosition().set(x, y);
		e.setAngle(angle);
	}
	
	public void updateBullet(int id, float x, float y) {
		Bullet b = this.bullets.get(this.getBulletIndex(id));
		b.getPosition().set(x, y);
	}
	
	public int getEnemyIndex(int id) {
		int index = 0;
		for(Enemy e : enemies) {
			if(e.getId() == id)
				break;
			index++;
		}
		return index;
	}

	public int getPlayerIndex(String username) {
		int index = 0;
		for(Player p : players) {
			if(p instanceof PlayerNet && ((PlayerNet) p).getUsername().equals(username))
				break;
			index++;
		}
		return index;
	}
	
	public int getBulletIndex(int id) {
		int index = 0;
		for(Bullet b : bullets) {
			if(b.getId() == id)
				break;
			index++;
		}
		return index;
	}
	
	public boolean playerExists(String username){
		if(username == null || username.equals(localPlayer.getUsername()))
			return false;
		else
			for(Player p : players)
				if(p instanceof PlayerNet && ((PlayerNet) p).getUsername().equals(username))
					return true;
		
		return false;
	}
	
	public boolean enemyExists(int id){
		for(Enemy e : enemies)
			if(e.getId() == id)
				return true;
		return false;
	}
	
	public boolean bulletExists(int id){
		for(Bullet b : bullets)
			if(b.getId() == id)
				return true;
		return false;
	}
	

	public TiledMap getTiledMap() {
		return this.map;
	}


	public Vector2 getMapSpawn() {
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(1);

		for(int i = 0; i < layer.getHeight(); i++)
			for(int j = 0; j < layer.getWidth(); j++)
				if(layer.getCell(j, i)!= null)
					if(layer.getCell(j, i).getTile().getProperties().containsKey("spawn_player"))
						return new Vector2(j * layer.getTileHeight(), i * layer.getTileWidth());
		
		return new Vector2(0,0);
	}
	
	public Array<EnemySpawn> getEnemiesSpawns() {
		Array<EnemySpawn> array = new Array<EnemySpawn>();
		
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(1);
		
		for(int i = 0; i < layer.getHeight(); i++)
			for(int j = 0; j < layer.getWidth(); j++)
				if(layer.getCell(j, i)!= null)
					if(layer.getCell(j, i).getTile().getProperties().containsKey("spawn_zombie"))
						array.add(new EnemySpawn(j * layer.getTileWidth(), i * layer.getTileHeight()));
		
		return array;
	}

	public void eraseEnemy(int id) {
		if(enemyExists(id))
			enemiesToDelete.add(enemies.get(this.getEnemyIndex(id)));
	}
	
	public void eraseBullet(int id) {
		if(bulletExists(id))
			bullets.remove(this.getBulletIndex(id));
	}

	public void addBullet(Bullet b) {
		this.bullets.add(b);
	}

	public void deleteEntity(int id) {
		if(enemyExists(id))
			eraseEnemy(id);
		else if(bulletExists(id))
			eraseBullet(id);
	}

	public void updateEntity(int id, float x, float y, float angle) {
		if(bulletExists(id))
			updateBullet(id, x, y);
		else if(enemyExists(id))
			updateEnemy(id, x, y, angle);

	}
}
