/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class MainMenu implements Screen{

	private Stage stage;
	private TextButton newGame, joinGame;
	private Skin uiSkin;
	
	public MainMenu() {
		stage = new Stage();
		uiSkin = new Skin(	Gdx.files.internal("skins/UI.json"),
							new TextureAtlas(Gdx.files.internal("skins/UI.pack")));
		
		 
		newGame = new TextButton("Host game", uiSkin);
		newGame.addListener(new ClickListener() {
		 	@Override
	        public void clicked(InputEvent event, float x, float y) {

	            ((Game)Gdx.app.getApplicationListener()).setScreen(new HostTransitionScreen());
	        }
			});
			newGame.setHeight(50);
			newGame.setPosition(10, 10);
			
		joinGame = new TextButton("Join game", uiSkin);
		joinGame.addListener(new ClickListener() {
		 	@Override
	        public void clicked(InputEvent event, float x, float y) {

	            ((Game)Gdx.app.getApplicationListener()).setScreen(new JoinTransitionScreen());
	        }
			});
			joinGame.setHeight(50);
			joinGame.setPosition(10, 70);
			
		stage.addActor(newGame);
		stage.addActor(joinGame);
		
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		
		stage.act();
		stage.draw();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
