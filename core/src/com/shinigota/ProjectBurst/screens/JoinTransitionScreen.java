/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.shinigota.ProjectBurst.tools.TextInput;

public class JoinTransitionScreen implements Screen, InputProcessor{
	
	private Stage stage;
	private TextButton startButton;
	private Label nameLabel, ipLabel;
	private TextInput nameField, ipField;
	private Skin uiSkin;
	private InputMultiplexer multiplexer;
	
	
	public JoinTransitionScreen() {
		multiplexer = new InputMultiplexer();
		stage = new Stage();
		uiSkin = new Skin(	Gdx.files.internal("skins/UI.json"),
							new TextureAtlas(Gdx.files.internal("skins/UI.pack")));
		
		 
		startButton = new TextButton("Start game", uiSkin);
		startButton.addListener(new ClickListener() {
		 	@Override
	        public void clicked(InputEvent event, float x, float y) {

	            ((Game)Gdx.app.getApplicationListener()).setScreen(new LoadingScreen(nameField.getText().toString(), ipField.getText().toString()));
	        }
			});
		startButton.setHeight(50);
			startButton.setPosition(10, 10);
			
		nameLabel = new Label("Name :" , uiSkin);
		nameLabel.setPosition(10, Gdx.graphics.getHeight() - nameLabel.getHeight() - 10);
		
		nameField = new TextInput("player", uiSkin);
		nameField.setPosition(nameLabel.getX() + nameLabel.getWidth() + 10, nameLabel.getY());
		
		ipLabel = new Label("Ip :" , uiSkin);
		ipLabel.setPosition(10, nameLabel.getY() - ipLabel.getHeight() - 10);
		
		ipField = new TextInput("87.91.197.232", uiSkin);
		ipField.setPosition(ipLabel.getX() + ipLabel.getWidth() + 10, ipLabel.getY());
		
		stage.addActor(nameLabel);
		stage.addActor(nameField);
		stage.addActor(ipLabel);
		stage.addActor(ipField);
		stage.addActor(startButton);
		
		multiplexer.addProcessor(stage);
		multiplexer.addProcessor(this);
		
		Gdx.input.setInputProcessor(multiplexer);
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 1);
		
		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
			if(nameField.contains(Gdx.input.getX(), Gdx.graphics.getHeight() - Gdx.input.getY())) {
				nameField.focus();
				ipField.stopFocus();
			} else if(ipField.contains(Gdx.input.getX(), Gdx.graphics.getHeight() - Gdx.input.getY())) {
				ipField.focus();
				nameField.stopFocus();
			} 
		}
		stage.act();
		stage.draw();
		
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch(keycode) {
		case Keys.BACKSPACE:
			if(nameField.isFocused())
				nameField.deleteChar();
			else if(ipField.isFocused())
				ipField.deleteChar();
			break;
		case Keys.RIGHT:
			if(nameField.isFocused())
				nameField.nextChar();
			else if(ipField.isFocused())
				ipField.nextChar();
			break;
		case Keys.LEFT:
			if(nameField.isFocused())
				nameField.previousChar();
			else if(ipField.isFocused())
				ipField.previousChar();
			break;
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		if(String.valueOf(character).matches("[A-Za-z0-9]") && nameField.isFocused())
			nameField.addChar(character);
		else if(String.valueOf(character).matches("[A-Za-z0-9.:]") && ipField.isFocused())
			ipField.addChar(character);
				
		return true;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
