/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.shinigota.ProjectBurst.net.ServerOptions;
import com.shinigota.ProjectBurst.tools.TextInput;

public class HostTransitionScreen implements Screen, InputProcessor{
	private Stage stage;
	private TextButton startButton;
	private Label nameLabel, mapLabel, selectedMapLabel;
	private TextInput nameField;
//	private ScrollList mapList;
	private Skin uiSkin;
	private InputMultiplexer multiplexer;
	private List<String> mapList;
	private ScrollPane scrollPane;
	
	int i = 0;
	
	public HostTransitionScreen() {
		multiplexer = new InputMultiplexer();
		stage = new Stage();
		uiSkin = new Skin(	Gdx.files.internal("skins/UI.json"),
							new TextureAtlas(Gdx.files.internal("skins/UI.pack")));
		
		 
		startButton = new TextButton("Start game", uiSkin);
		startButton.addListener(new ClickListener() {
		 	@Override
	        public void clicked(InputEvent event, float x, float y) {
		 			
	            ((Game)Gdx.app.getApplicationListener()).setScreen(new LoadingScreen(nameField.getText().toString(), new ServerOptions("maps/" + selectedMapLabel.getText())));
	        }
			});
		startButton.setHeight(50);
			startButton.setPosition(10, 10);
			
		nameLabel = new Label("Name :" , uiSkin);
		nameLabel.setPosition(10, Gdx.graphics.getHeight() - nameLabel.getHeight() - 10);
		
		nameField = new TextInput("host", uiSkin);
		nameField.setPosition(nameLabel.getX() + nameLabel.getWidth() + 10, nameLabel.getY());
		
		mapLabel = new Label("Mapp :" , uiSkin);
		mapLabel.setPosition(10, nameLabel.getY() - mapLabel.getHeight() - 10);
		
		selectedMapLabel = new Label(" " , uiSkin, "input_label");
		selectedMapLabel.setPosition(mapLabel.getX() + mapLabel.getWidth() + 10, mapLabel.getY());
		selectedMapLabel.setWidth(250);
		
		mapList = new List<String>(uiSkin);
		String[] test = this.getMaps();
		mapList.setItems(test);
		mapList.addListener(new ClickListener(){
			 @Override
			 public void clicked(InputEvent event, float x, float y) {
		        selectedMapLabel.setText(mapList.getSelected());
		    };
		});
		scrollPane = new ScrollPane(mapList, uiSkin);
		scrollPane.setBounds(selectedMapLabel.getX(), selectedMapLabel.getY() - 100, selectedMapLabel.getWidth(), 100);

		
		/*mapList = new ScrollList("Choose map", uiSkin);
		mapList.setPosition(mapLabel.getX() + mapLabel.getWidth() + 10, mapLabel.getY());
		mapList.setWidth(200);
		mapList.init();*/
		
		stage.addActor(nameLabel);
		stage.addActor(nameField);
		stage.addActor(mapLabel);
		stage.addActor(selectedMapLabel);
		stage.addActor(scrollPane);
		stage.addActor(startButton);
		
		multiplexer.addProcessor(stage);
		multiplexer.addProcessor(this);
		
		Gdx.input.setInputProcessor(multiplexer);
		
		getMaps();
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 1);
		
		stage.act();
		stage.draw();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

	private String[] getMaps() {
		FileHandle[] files = Gdx.files.internal(Gdx.files.getLocalStoragePath()+ "/maps/").list();
		Array<String> maps = new Array<String>();
		
		for(FileHandle f : files)
			if(f.extension().equals("tmx"))
				maps.add(f.name());
		
		String[] result = new String[maps.size];
		for(int i = 0; i < maps.size; i++)
			result[i] = maps.get(i);
		
		return result;
	}
	
	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch(keycode) {
		case Keys.BACKSPACE:
			nameField.deleteChar();
			break;
		case Keys.RIGHT:
			nameField.nextChar();
			break;
		case Keys.LEFT:
			nameField.previousChar();
			break;
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		if(String.valueOf(character).matches("[A-Za-z0-9]"))
			nameField.addChar(character);
				
		return true;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
