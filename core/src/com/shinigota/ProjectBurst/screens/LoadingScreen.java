/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.math.Vector2;
import com.shinigota.ProjectBurst.entities.Entity;
import com.shinigota.ProjectBurst.entities.net.PlayerNet;
import com.shinigota.ProjectBurst.net.GameClient;
import com.shinigota.ProjectBurst.net.GameServer;
import com.shinigota.ProjectBurst.net.ServerOptions;
import com.shinigota.ProjectBurst.packets.PacketAskInfo;
import com.shinigota.ProjectBurst.packets.PacketConnect;
import com.shinigota.ProjectBurst.world.World;

public class LoadingScreen implements Screen{

	private World world;
	private GameClient client;
	private GameServer server;
	private PlayerNet player;
	private boolean loadingFinished = false;;
	
	public LoadingScreen(String username, ServerOptions options) {
		this.world = new World(options.mapName);
		
		this.server = new GameServer(world, options);
		this.server.start();
		
		this.client = new GameClient(world, "localhost");
		this.client.start();
		
		Vector2 spawn= this.world.getMapSpawn();
		player = new PlayerNet((int)spawn.x, (int) spawn.y, Entity.DEF_WIDTH, Entity.DEF_HEIGHT, null, -1, username);
		world.addLocalPlayer(player);
		
		PacketConnect login = new PacketConnect(player.getUsername());

		server.addConnection(player, login);
		login.writeData(client);
		
		this.loadingFinished = true;
	}
	
	public LoadingScreen(String username, String hostAddress) {
		this.server = null;
		
		this.client = new GameClient(hostAddress);
		this.client.start();
		
		PacketAskInfo packetAsk = new PacketAskInfo();
		packetAsk.writeData(client);
		
		while(client.getServerOptions() == null);
		this.world = new World(client.getServerOptions().mapName);
		this.client.setWorld(this.world);
		
		Vector2 spawn= this.world.getMapSpawn();
		player = new PlayerNet((int) spawn.x, (int) spawn.y, Entity.DEF_WIDTH, Entity.DEF_HEIGHT, null, -1, username);
		world.addLocalPlayer(player);
		
		PacketConnect login = new PacketConnect(player.getUsername());
		login.writeData(client);
		
		this.loadingFinished = true;

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		
		if(client.isReady() && this.loadingFinished)
			((Game) Gdx.app.getApplicationListener()).setScreen(new GameScreen(client, server, world, player));
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
}
