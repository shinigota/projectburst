/*******************************************************************************
 *     ProjectBurst, multiplayer suvival game
 *     Copyright (C) 2015  Benjamin Barbe
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.shinigota.ProjectBurst.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.shinigota.ProjectBurst.entities.net.PlayerNet;
import com.shinigota.ProjectBurst.map.TiledMapCollider;
import com.shinigota.ProjectBurst.net.GameClient;
import com.shinigota.ProjectBurst.net.GameServer;
import com.shinigota.ProjectBurst.packets.PacketDisconnect;
import com.shinigota.ProjectBurst.tools.PlayerInputProcessor;
import com.shinigota.ProjectBurst.tools.ScreenInputProcessor;
import com.shinigota.ProjectBurst.world.World;
import com.shinigota.ProjectBurst.world.WorldRenderer;

public class GameScreen implements Screen{
	public static GameScreen game;
	public static TiledMapCollider mapCollider;
	
	public GameClient client;
	public GameServer server;
	
	private World world;
	private WorldRenderer renderer;
	
	private PlayerNet player;
	
	private PlayerInputProcessor pInput;
	private ScreenInputProcessor sInput;
	
	private InputMultiplexer multiplexer;
	
//	public GameScreen(String username, ServerOptions options) {
//		this.game = this;
//		
//		this.world = new World(options.mapName);
//		this.renderer = new WorldRenderer(world);
//		
//		this.server = new GameServer(world);
//		this.server.start();
//		
//		this.client = new GameClient(world, "localhost");
//		this.client.start();
//		
//		player = new PlayerNet(800, 800, Player.WIDTH, Player.HEIGHT, null, -1, username);
//		world.addLocalPlayer(player);
//		
//		PacketConnect login = new PacketConnect(player.getUsername());
//
//		server.addConnection(player, login);
//		
//		pInput = new PlayerInputProcessor(this.player, this.renderer.getCam());
//		sInput = new ScreenInputProcessor();
//		multiplexer = new InputMultiplexer();
//		multiplexer.addProcessor(pInput);
//		multiplexer.addProcessor(sInput);
//		Gdx.input.setInputProcessor(multiplexer);
//		
//		login.writeData(client);
//	}
//	
//	public GameScreen(String username, String hostAddress) {
//		this.game = this;
//		
//		this.world = new World();
//		this.renderer = new WorldRenderer(world);
//		
//		this.client = new GameClient(world, hostAddress);
//		this.client.start();
//		
//		player = new PlayerNet(800, 800, Player.WIDTH, Player.HEIGHT, null, -1, username);
//		world.addLocalPlayer(player);
//		
//		PacketConnect login = new PacketConnect(player.getUsername());
//		
//		pInput = new PlayerInputProcessor(this.player, this.renderer.getCam());
//		sInput = new ScreenInputProcessor();
//		multiplexer = new InputMultiplexer();
//		multiplexer.addProcessor(pInput);
//		multiplexer.addProcessor(sInput);
//		Gdx.input.setInputProcessor(multiplexer);
//		
//		login.writeData(client);
//	}
	
	public GameScreen(GameClient client, GameServer server, World world, PlayerNet player) {

		GameScreen.game = this;
		
		this.world = world;
		this.renderer = new WorldRenderer(world);
		
		this.client = client;
		this.player = player;
		
		TiledMapCollider.init((TiledMapTileLayer) world.getTiledMap().getLayers().get(0), world);
		pInput = new PlayerInputProcessor(this.player, this.renderer.getCam());
		sInput = new ScreenInputProcessor();
		multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(pInput);
		multiplexer.addProcessor(sInput);
		Gdx.input.setInputProcessor(multiplexer);
	}
	

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		world.update(delta);
		renderer.render(delta);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		PacketDisconnect packet = new PacketDisconnect(player.getUsername());
		packet.writeData(client);
		
		renderer.dispose();
	}

}
